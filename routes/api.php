<?php

 /*
 |--------------------------------------------------------------------------
 | Файл маршрутизации SDFramework
 |--------------------------------------------------------------------------
 |
 | Нужен для реализация маршрутов внутри ресурса
 | Подробнее: 
 |
 */

namespace Roast\Routes;
$route->get('/', '\Roast\Controllers\Regular\DefaultController@welcome');

$route->get('/api/enter/(\w+)', function($login) {
    echo \Roast\Controllers\Regular\DefaultController::getUser($login);
});

$route->post('/api/task', function() {
    echo \Roast\Controllers\Regular\DefaultController::createTask();
});

$route->get('/api/get_tasks/(\w+)', function($user_id) {
    echo \Roast\Controllers\Regular\DefaultController::getTasks($user_id);
});

$route->get('/api/get_deals/(\w+)', function($user_id) {
    echo \Roast\Controllers\Regular\DefaultController::getDealsByUser($user_id);
});

$route->get('/api/get_user_by_id/(\w+)', function($user_id) {
    echo \Roast\Controllers\Regular\DefaultController::getUserById($user_id);
});

$route->get('/api/get_user_by_root/(\w+)', function($root) {
    echo \Roast\Controllers\Regular\DefaultController::getUserByRoot($root);
});

$route->post('/api/create_rqst', function() {
    echo \Roast\Controllers\Regular\DefaultController::createRqst();
});

$route->post('/api/change_user_rate', function() {
    echo \Roast\Controllers\Regular\DefaultController::changeUserRate();
});

$route->post('/api/change_call_status', function() {
    echo \Roast\Controllers\Regular\DefaultController::changeCallStatus();
});

$route->post('/api/create_deal', function() {
    echo \Roast\Controllers\Regular\DefaultController::createDeal();
});

$route->get('/api/get_calls', function() {
    echo \Roast\Controllers\Regular\DefaultController::getCalls();
});

$route->get('/api/get_request', function() {
    echo \Roast\Controllers\Regular\DefaultController::getRequest();
});




