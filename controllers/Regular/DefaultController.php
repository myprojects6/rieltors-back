<?php 
namespace Roast\Controllers\Regular;
use Roast\App;
use Roast\Controller;
use Roast\Response;
use Roast\DatabaseORM;

class DefaultController extends Controller
{

   /**
    * welcome
    * @return void
    */
   public static function welcome()
   {
     return Response::JSON(App::Ver());
   }

    public static function getUser($login)
    {
        $db = (new DatabaseORM())->ORM;
        $data = $db->getAll("SELECT * FROM users WHERE login = '".$login."'");
        return json_encode($data);
    }

    public static function getCalls()
    {
        $db = (new DatabaseORM())->ORM;
        $data = $db->getAll("SELECT * FROM connects");
        return json_encode($data);
    }


    public static function getRequest()
    {
        $db = (new DatabaseORM())->ORM;
        $data = $db->getAll("SELECT * FROM suers");
        return json_encode($data);
    }




    public static function createTask()
    {
        $db = (new DatabaseORM())->ORM;
        $data = $_POST;
        $tracker = $db->dispense('tracker');
        $tracker->user_id = $data['user_id'];
        $tracker->day = $data['day'];
        $tracker->month = $data['month'];
        $tracker->year = $data['year'];
        $tracker->time = $data['time'];
        $tracker->name = $data['name'];
        $tracker->type = $data['type'];
        // Сохраняем объект
        $db->store($tracker);
        return json_encode($data);
    }

    public static function getTasks($user_id)
    {
        $db = (new DatabaseORM())->ORM;
        $data = $db->getAll("SELECT * FROM tracker WHERE user_id='".$user_id."'");
        return json_encode($data);
    }

    public static function getDealsByUser($user_id)
    {
        $db = (new DatabaseORM())->ORM;
        $data = $db->getAll("SELECT * FROM deals WHERE user_id='".$user_id."'");
        return json_encode($data);
    }

    public static function getUserById($user_id)
    {
        $db = (new DatabaseORM())->ORM;
        $data = $db->getAll("SELECT * FROM users WHERE id='".$user_id."'");
        return json_encode($data);
    }

    public static function getUserByRoot($root)
    {
        $db = (new DatabaseORM())->ORM;
        $data = $db->getAll("SELECT * FROM users WHERE root='".$root."'");
        return json_encode($data);
    }

   public static function createRqst()
      {
        $db = (new DatabaseORM())->ORM;
        $data = $_POST;
        $connect = $db->dispense('connects');
        $connect->connect_name = $data['connect_name'];
        $connect->connect_data = $data['connect_data'];
        $connect->connect_time = $data['connect_time'];
        $connect->connect_date = $data['connect_date'];
        $db->store($connect);
        return json_encode($data);
      }
    public static function createDeal()
    {
        $db = (new DatabaseORM())->ORM;
        $data = $_POST;
        $deals = $db->dispense('deals');
        $deals->user_id = $data['user_id'];
        $deals->deal_name = $data['deal_name'];
        $deals->deal_date = $data['deal_date'];
        $deals->deal_price = $data['deal_price'];
        $deals->deal_object = $data['deal_object'];
        $deals->deal_place = $data['deal_place'];
        $db->store($deals);
        return json_encode($data);
    }
    public static function changeUserRate()
    {
        $db = (new DatabaseORM())->ORM;
        $data = $_POST;
        $id = $data['id'];
        $user = $db->load('users', $id);
        // Заполняем объект свойствами
        $user->rate = $data['rate'];
        // Сохраняем объект
        $db->store($user);
        return json_encode($data);
    }
    public static function changeCallStatus()
    {
        $db = (new DatabaseORM())->ORM;
        $data = $_POST;
        $id = $data['id'];
        $connect = $db->load('connects', $id);
        // Заполняем объект свойствами
        $connect->status = $data['status'];
        // Сохраняем объект
        $db->store($connect);
        return json_encode($data);
    }
}
